#ifndef I2C_SUBJECT_H
#define I2C_SUBJECT_H

#include <stdint.h>

typedef uint8_t (*sensor_update_function_type)(uint8_t arg);

uint8_t add_sensor_observer(sensor_update_function_type sensor_observer);
uint8_t remove_sensor_observer(sensor_update_function_type sensor_observer);
uint8_t notify_sensor_observers(uint8_t arg);
uint8_t cleanup_subject();

#endif