#include <stdlib.h>
#include "i2c_subject.h"

//dynamic array of subscribed observers
sensor_update_function_type *observer_array;
//number of subscribed observers
uint8_t subscribed_observers = 0;

/**
 * This function adds an observer to the array of observers
 */
uint8_t add_sensor_observer(sensor_update_function_type sensor_observer)
{
    //if there is no subscribed observer yet allocate memory for observer array
    if (subscribed_observers == 0)
    {
        observer_array = malloc(sizeof(sensor_update_function_type));
    }
    //if there are subscribed observers reallocate memory for observer array
    else
    {
        observer_array = realloc(observer_array, ((subscribed_observers + 1) * sizeof(sensor_update_function_type)));
    }

    //add new observer to array and increase number of subscribed observers
    observer_array[subscribed_observers] = sensor_observer;
    subscribed_observers++;

    return 0;
}

/**
 * This function removes an observer from the array of observers
 */
uint8_t remove_sensor_observer(sensor_update_function_type sensor_observer)
{
    //determine index of observer that should be removed
    int index_of_observer_to_remove = -1;
    for (int i = 0; i < subscribed_observers; i++)
    {
        if (observer_array[i] == sensor_observer)
        {
            index_of_observer_to_remove = i;
            break;
        }
    }

    //new observer array
    sensor_update_function_type *new_observer_array;
    new_observer_array = malloc(((subscribed_observers - 1) * sizeof(sensor_update_function_type)));

    //copy old observer array except the one that should be removed
    int new_observer_array_index = 0;
    for (int i = 0; i < subscribed_observers; i++)
    {
        //skip the one that should be removed
        if (i == index_of_observer_to_remove)
        {
            continue;
        }
        new_observer_array[new_observer_array_index] = observer_array[i];
        new_observer_array_index++;
    }

    //free memory of old observer array
    free(observer_array);
    //set observer array to new observer array
    observer_array = new_observer_array;

    subscribed_observers--;

    return 0;
}

/**
 * This function notifies all subscribed observers by calling their update-function
 * The function could be triggered periodically or by a user input
 */
uint8_t notify_sensor_observers(uint8_t arg)
{
    for (int i = 0; i < subscribed_observers; i++)
    {
        (*observer_array[i])(arg);
    }

    return 0;
}

/**
 * This function frees the memory allocated by the observer array
 */
uint8_t cleanup_subject()
{
    free(observer_array);

    return 0;
}