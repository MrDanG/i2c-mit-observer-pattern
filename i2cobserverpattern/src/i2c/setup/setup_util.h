#ifndef SETUP_UTIL_H
#define SETUP_UTIL_H

#include "../i2c.h"

#define NORMALMODE      0 //100kbps
#define FASTMODE        1 //400kbps
#define FASTMODEPLUS    2 //1Mbps



void init_i2c(int bus_speed, int sda_pin, int scl_pin) ;

#endif