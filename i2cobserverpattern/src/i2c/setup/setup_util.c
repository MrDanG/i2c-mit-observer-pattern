#include "setup_util.h"

#define CLOCK_STRETCH_TIMEOUT 15000
#define PORT_CONFIG_VALUE 3 | 0x10
#define PORT_CONTROL_VALUE (IOMUXC_PAD_ODE | IOMUXC_PAD_SRE | IOMUXC_PAD_DSE(4) | IOMUXC_PAD_SPEED(1) | IOMUXC_PAD_PKE | IOMUXC_PAD_PUE | IOMUXC_PAD_PUS(3))

/**
 * Enables the i2c clock that operates with 24MHz
 * The folowing registers form the Clock Controller Module (CCM) are used:
 * Serial Clock Divider Register 2 (CCM_CSCDR2)
 * Clock Gating Register 2 (CCM_CCGR2)
 */
void enable_i2c_clock()
{
    //set i2c clock divider to 1
    CCM_CSCDR2 &= ~CCM_CSCDR2_LPI2C_CLK_PODF(63);
    //derive clock from osc_clk which is a 24MHz clock
    CCM_CSCDR2 |= CCM_CSCDR2_LPI2C_CLK_SEL;
    //enables the first i2c clock
    CCM_CCGR2 |= CCM_CCGR2_LPI2C1(CCM_CCGR_ON);
}

/**
 * Configures the bus speed
 * The following ports of the Low Power Inter-Integrated Circuit (LPI2C) are used:
 * Master Clock Configuration Register 0 (MCCR0)
 * Master Clock Configuration Register 1 (MCCR1)
 * Master Configuration Register 0 (MCFGR0)
 * Master Configuration Register 1 (MCFGR1)
 * Master Configuration Register 2 (MCFGR2)
 * Master Configuration Register 3 (MCFGR3)
 * Master FIFO Control Register (MFCR)
 * Master Control Register (MCR)
 */
void set_bus_speed(int mode)
{
    //reset MCR, especially disable master mode for configuration
    I2C->MCR = 0;
    //setup bus speed depending on wanted mode
    switch (mode)
    {
    case FASTMODE:
        I2C->MCCR0 |= LPI2C_MCCR0_CLKHI(0x35);
        I2C->MCCR0 |= LPI2C_MCCR0_CLKLO(0x3E);
        I2C->MCCR0 |= LPI2C_MCCR0_DATAVD(0x0F);
        I2C->MCCR0 |= LPI2C_MCCR0_SETHOLD(0x1D);
        I2C->MCFGR1 |= LPI2C_MCFGR1_PRESCALE(0x00);
        I2C->MCFGR2 |= LPI2C_MCFGR2_FILTSDA(0x01);
        I2C->MCFGR2 |= LPI2C_MCFGR2_FILTSCL(0x01);
        //(0x3E + 0x1D + 2) * 2 = (62 + 29 + 2) * 2 = 186 = 0xBA
        I2C->MCFGR2 |= LPI2C_MCFGR2_BUSIDLE(0xBA);
        I2C->MCFGR3 = LPI2C_MCFGR3_PINLOW(CLOCK_STRETCH_TIMEOUT * 24 / 256 + 1);
        break;
    case FASTMODEPLUS:
        I2C->MCCR0 |= LPI2C_MCCR0_CLKHI(0x04);
        I2C->MCCR0 |= LPI2C_MCCR0_CLKLO(0x06);
        I2C->MCCR0 |= LPI2C_MCCR0_DATAVD(0x04);
        I2C->MCCR0 |= LPI2C_MCCR0_SETHOLD(0x03);
        I2C->MCFGR1 |= LPI2C_MCFGR1_PRESCALE(0x02);
        I2C->MCFGR2 |= LPI2C_MCFGR2_FILTSDA(0x01);
        I2C->MCFGR2 |= LPI2C_MCFGR2_FILTSCL(0x01);
        //(0x06 + 0x03 + 2) * 2 = (6 + 3 + 2) * 2 = 22 = 0x16
        I2C->MCFGR2 |= LPI2C_MCFGR2_BUSIDLE(0x16);
        I2C->MCFGR3 = LPI2C_MCFGR3_PINLOW(CLOCK_STRETCH_TIMEOUT * 48 / 256 + 1);
        break;
    case NORMALMODE:
    //there is no code or break on purpose
    //the normal mode has the same configuration as the default configuration
    default:
        //low period of SCL
        I2C->MCCR0 |= LPI2C_MCCR0_CLKHI(0x37);
        //high period of SCL
        I2C->MCCR0 |= LPI2C_MCCR0_CLKLO(0x3B);
        //data valid delay
        I2C->MCCR0 |= LPI2C_MCCR0_DATAVD(0x19);
        //setup hold delay
        I2C->MCCR0 |= LPI2C_MCCR0_SETHOLD(0x28);
        //clock prescaler
        I2C->MCFGR1 |= LPI2C_MCFGR1_PRESCALE(0x01);
        //glitch filter SDA
        I2C->MCFGR2 |= LPI2C_MCFGR2_FILTSDA(0x05);
        //glitch filter SCL
        I2C->MCFGR2 |= LPI2C_MCFGR2_FILTSCL(0x05);
        //bus idle timeout, min: (CLKLO + SETHOLD + 2) * 2
        //(0x3B + 0x28 + 2) * 2 = (59 + 25 + 2) * 2 = 172 = 0xAC
        I2C->MCFGR2 |= LPI2C_MCFGR2_BUSIDLE(0xAC);
        //pin low timeout
        I2C->MCFGR3 = LPI2C_MCFGR3_PINLOW(CLOCK_STRETCH_TIMEOUT * 12 / 256 + 1);
        break;
    }
    //set MCCR1 to same value as MCCR0
    //could be set to a different speed e.g. high speed mode for switching between MCCR1 and MCCR2
    I2C->MCCR1 |= I2C->MCCR0;
    //reset the MCFGR0 register
    I2C->MCFGR0 = 0;
    //set Receive FIFO Watermark so the Receive Data Flag is set when the number of words in the
    //Receive FIFO is greater then the RXWATER
    I2C->MFCR |= LPI2C_MFCR_RXWATER(0x01);
    //set Transmit FIFO Watermark so the Transmit Data Flag is set when the number of words in the
    //Transmit FIFO is greater then the TXWATER
    I2C->MFCR |= LPI2C_MFCR_TXWATER(0x01);
    //enable master mode
    I2C->MCR |= LPI2C_MCR_MEN;
}

/**
 * configures the SDA and SCL pins
 */
void configure_ports(int sda_pin, int scl_pin)
{
    /**
     * portControlRegister() und portConfigRegister() geben das config und control register zu einem pin zurück
     * die i2c pins sind 18 (sda)   ->control   IOMUXC_SW_PAD_CTL_PAD_GPIO_AD_B1_01
     *                              ->config    IOMUXC_SW_MUX_CTL_PAD_GPIO_AD_B1_01
     * 19 (scl)                     ->control   IOMUXC_SW_PAD_CTL_PAD_GPIO_AD_B1_00
     *                              ->config    IOMUXC_SW_MUX_CTL_PAD_GPIO_AD_B1_00
     * 17 (sda-1)                   ->control   IOMUXC_SW_PAD_CTL_PAD_GPIO_AD_B1_06
     *                              ->config    IOMUXC_SW_MUX_CTL_PAD_GPIO_AD_B1_06
     * 16 (scl-1)                   ->control   IOMUXC_SW_PAD_CTL_PAD_GPIO_AD_B1_07
     *                              ->config    IOMUXC_SW_MUX_CTL_PAD_GPIO_AD_B1_07
     * aus diesen registern ergeben sich die werte die in WireIMXRT festgelegt sind (pad, mux, usw.)
     */
    *(portControlRegister(sda_pin)) = PORT_CONTROL_VALUE; //18
    *(portConfigRegister(sda_pin)) = PORT_CONFIG_VALUE;   //18
    IOMUXC_LPI2C1_SDA_SELECT_INPUT = 0x01;

    *(portControlRegister(scl_pin)) = PORT_CONTROL_VALUE; //19
    *(portConfigRegister(scl_pin)) = PORT_CONFIG_VALUE;   //19
    IOMUXC_LPI2C1_SCL_SELECT_INPUT = 0x01;
}

/**
 * Initialise the i2c bus
 * bus_speed:   wanted bus speed (NORMALMODE, FASTMODE, FASTMODEPLUS)
 * sda_pin:     number of sda pin (SDA_PIN_1, SDA_PIN_2)
 * scl_pin:     number of scl pin (SCL_PIN_1, SCL_PIN_2)
 */
void init_i2c(int bus_speed, int sda_pin, int scl_pin)
{
    enable_i2c_clock();
    set_bus_speed(bus_speed);
    configure_ports(sda_pin, scl_pin);
}
