#include "status_util.h"

uint32_t get_status_register(){
    return I2C->MSR;
}

bool is_bus_busy(){
    if(get_status_register() & LPI2C_MSR_BBF){
        return true;
    }
    return false;
}

bool is_master_busy(){
    if(get_status_register() & LPI2C_MSR_MBF){
        return true;
    }
    return false;
}

bool is_received_data_match(){
    if(get_status_register() & LPI2C_MSR_DMF){
        return true;
    }
    return false;
}

bool is_pin_low_timeout(){
    if(get_status_register() & LPI2C_MSR_PLTF){
        return true;
    }
    return false;
}

bool is_transmit_receive_error(){
    if(get_status_register() & LPI2C_MSR_FEF){
        return true;
    }
    return false;
}

bool is_arbitration_lost(){
    if(get_status_register() & LPI2C_MSR_ALF){
        return true;
    }
    return false;
}

bool is_nack_detected(){
    if(get_status_register() & LPI2C_MSR_NDF){
        return true;
    }
    return false;
}

bool is_stop_detected(){
    if(get_status_register() & LPI2C_MSR_SDF){
        return true;
    }
    return false;
}

bool is_end_packet(){
    if(get_status_register() & LPI2C_MSR_EPF){
        return true;
    }
    return false;
}

bool is_receive_data_to_large(){
    if(get_status_register() & LPI2C_MSR_RDF){
        return true;
    }
    return false;
}

bool is_transmit_data_to_large(){
    if(get_status_register() & LPI2C_MSR_TDF){
        return true;
    }
    return false;
}
