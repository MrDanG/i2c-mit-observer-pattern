#ifndef STATUS_UTIL_H
#define STATUS_UTIL_H

#include "../i2c.h"

uint32_t get_status_register();

bool is_bus_busy();
bool is_master_busy();
bool is_received_data_match();
bool is_pin_low_timeout();
bool is_transmit_receive_error();
bool is_arbitration_lost();
bool is_nack_detected();
bool is_stop_detected();
bool is_end_packet();
bool is_receive_data_to_large();
bool is_transmit_data_to_large();

#endif