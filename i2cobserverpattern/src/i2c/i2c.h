#ifndef I2C_H
#define I2C_H

#include <Arduino.h>
#include <imxrt.h>
#include <stdbool.h>

#define SDA_PIN_1       18 
#define SCL_PIN_1       19
#define SDA_PIN_2       
#define SCL_PIN_2

static IMXRT_LPI2C_t * const I2C = &IMXRT_LPI2C1;

#endif