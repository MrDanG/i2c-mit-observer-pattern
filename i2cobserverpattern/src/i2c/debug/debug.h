#ifndef DEBUG_H
#define DEBUG_H

#include <Arduino.h>

#ifdef __cplusplus
extern "C" {
#endif
    void i2c_string_debug_print(char *message);
    void i2c_uint8_debug_print(uint8_t message);
    void i2c_uint32_debug_print(uint32_t message);
#ifdef __cplusplus
}
#endif

#endif