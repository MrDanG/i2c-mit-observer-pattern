#include "debug.h"

extern "C"
{
    void i2c_string_debug_print(char *message){
        Serial.println(message);
    }

    void i2c_uint8_debug_print(uint8_t message){
        Serial.println(message, BIN);
    }
    void i2c_uint32_debug_print(uint32_t message){
        Serial.println(message);
    }
}
