#include "i2c_time.h"
#include <elapsedMillis.h>
extern "C"
{
    elapsedMillis timer_array[MAX_NUMBER_OF_TIMERS] = {0};
    uint8_t time_list_length = 0;

    uint8_t generate_time()
    {
        elapsedMillis em = 0;
        uint8_t index = -1;
        for (uint8_t i = 0; i < MAX_NUMBER_OF_TIMERS; i++)
        {
            if (timer_array[i] == 0)
            {
                timer_array[i] = em;
                index = i;
            }
        }
        return index;
    }

    uint8_t delete_time(uint8_t index)
    {
        if(index == -1){
            return -1;
        }
        timer_array[index] = 0;
        return -1;
    }

    unsigned long get_time(uint8_t index)
    {
        if(index == -1){
            return -1;
        }
        return timer_array[index];
    }
}
