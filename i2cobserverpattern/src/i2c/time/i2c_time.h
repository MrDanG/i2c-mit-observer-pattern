#ifndef I2C_TIME_H
#define I2C_TIME_H

#include "../i2c.h"

#define MAX_NUMBER_OF_TIMERS 32

#ifdef __cplusplus
extern "C" {
#endif
    unsigned long get_time(uint8_t index);
    uint8_t generate_time();
    uint8_t delete_time(uint8_t index);
#ifdef __cplusplus
}
#endif

#endif