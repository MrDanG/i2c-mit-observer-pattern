#ifndef DATA_TRANSFER_UTIL_H
#define DATA_TRANSFER_UTIL_H

#include "../status/status_util.h"
#include "../time/i2c_time.h"
#include "../debug/debug.h"

#define RW_BIT_WRITE    0
#define RW_BIT_READ     1

void reset_transmit_fifo();
void reset_receive_fifo();
void reset_transmit_and_receive_fifo();
uint32_t get_words_in_transmit_fifo();
uint32_t get_words_in_receive_fifo();

bool wait_for_bus_idle();

void send_start_signal_with_address(uint8_t address, uint32_t send_receive);
void send_transmit_signal_with_data(uint8_t data);
void send_receive_signal_with_quantity(uint8_t receive_data_quantity);
void send_stop_signal();
uint8_t check_transmit_receive_error(uint8_t timer);

#endif