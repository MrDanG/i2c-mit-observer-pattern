#include "data_transfer_util.h"

void reset_transmit_fifo()
{
	I2C->MCR |= LPI2C_MCR_RTF;
}

void reset_receive_fifo()
{
	I2C->MCR |= LPI2C_MCR_RRF;
}

void reset_transmit_and_receive_fifo()
{
	reset_transmit_fifo();
	reset_receive_fifo();
}

uint32_t get_words_in_transmit_fifo()
{
	return I2C->MFSR & 0x07;
}

uint32_t get_words_in_receive_fifo()
{
	return (I2C->MFSR >> 16) & 0x07;
}

bool force_clock()
{
	bool ret = false;
	uint32_t sda_pin = SDA_PIN_1;
	uint32_t scl_pin = SCL_PIN_1;
	uint32_t sda_mask = digitalPinToBitMask(sda_pin);
	uint32_t scl_mask = digitalPinToBitMask(scl_pin);
	// take control of pins with GPIO
	*portConfigRegister(sda_pin) = 5 | 0x10;
	*portSetRegister(sda_pin) = sda_mask;
	*portModeRegister(sda_pin) |= sda_mask;
	*portConfigRegister(scl_pin) = 5 | 0x10;
	*portSetRegister(scl_pin) = scl_mask;
	*portModeRegister(scl_pin) |= scl_mask;
	delayMicroseconds(10);
	for (int i = 0; i < 9; i++)
	{
		if ((*portInputRegister(sda_pin) & sda_mask) && (*portInputRegister(scl_pin) & scl_mask))
		{
			// success, both pins are high
			ret = true;
			break;
		}
		*portClearRegister(scl_pin) = scl_mask;
		delayMicroseconds(5);
		*portSetRegister(scl_pin) = scl_mask;
		delayMicroseconds(5);
	}
	// return control of pins to I2C
	*(portConfigRegister(sda_pin)) = 3 | 0x10;
	*(portConfigRegister(scl_pin)) = 3 | 0x10;
	return ret;
}

bool wait_for_bus_idle()
{
	uint8_t timeout = generate_time();
	while (1)
	{
		if (!is_bus_busy())
		{
			break;
		}
		if (is_master_busy())
		{
			break;
		}
		if (get_time(timeout) > 16)
		{
			if (force_clock())
			{
				break;
			}
			return false;
		}
	}
	//what is this? needs to be changed
	//0x00007F00 = 0111 1111 0000 0000
	I2C->MSR = 0x00007F00; // clear all prior flags
	delete_time(timeout);
	return true;
}

uint8_t check_transmit_receive_error(uint8_t timer)
{
	if (is_arbitration_lost())
	{
		reset_transmit_and_receive_fifo();
		return 4;
	}
	if (is_nack_detected())
	{
		reset_transmit_and_receive_fifo();
		send_stop_signal();
		return 2;
	}
	if (is_pin_low_timeout() || get_time(timer) > 50)
	{
		reset_transmit_and_receive_fifo();
		send_stop_signal();
		return 4;
	}
	return 0;
}

void send_start_signal_with_address(uint8_t address, uint32_t send_receive)
{
	uint8_t i2c_address = (address & 0x7F) << 1;
	I2C->MTDR = LPI2C_MTDR_CMD_START | i2c_address | send_receive;
}

void send_transmit_signal_with_data(uint8_t data)
{
	I2C->MTDR = LPI2C_MTDR_CMD_TRANSMIT | data;
}

void send_stop_signal()
{
	I2C->MTDR = LPI2C_MTDR_CMD_STOP;
}

void send_receive_signal_with_quantity(uint8_t receive_data_quantity){
	I2C->MTDR = LPI2C_MTDR_CMD_RECEIVE | receive_data_quantity;
}