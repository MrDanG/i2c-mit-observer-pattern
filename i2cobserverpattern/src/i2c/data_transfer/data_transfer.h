#ifndef DATA_TRANSFER_H
#define DATA_TRANSFER_H

#include "data_transfer_util.h"

#define SEND_STOP_SIGNAL 1
#define SEND_NO_STOP_SIGNAL 0
#define NEW_TRANSMISSION 1
#define NO_NEW_TRANSMISSION 0

uint8_t receive_data(uint8_t address, uint8_t receive_data_quantity, uint8_t send_stop_signal_flag);
uint8_t transmit_data(uint8_t address, uint8_t send_stop_signal_flag);
int write_data_to_transmit_buffer(uint8_t data, uint8_t new_transmission_flag);
uint8_t read_data_from_receive_buffer();
uint8_t get_receive_buffer_quantity();

#endif