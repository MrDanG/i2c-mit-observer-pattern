#include "data_transfer.h"

#define BUFFER_LENGTH 32

uint8_t transmit_buffer[BUFFER_LENGTH] = {0};
uint8_t transmit_buffer_content_length = 0;

uint8_t receive_buffer[BUFFER_LENGTH] = {0};
uint8_t receive_buffer_content_length = 0;
uint8_t receive_buffer_global_index = 0;

uint8_t receive_data(uint8_t address, uint8_t receive_data_quantity, uint8_t send_stop_signal_flag)
{
    uint8_t transmit_state = 0;
    receive_buffer_content_length = 0;
    //bus is busy so no data can be send
    if (!wait_for_bus_idle())
    {
        return 4;
    }
    uint8_t timer = generate_time();
    while (1)
    {
        if (transmit_state < 3)
        {
            uint32_t words_in_transmit_fifo = get_words_in_transmit_fifo();
            while (words_in_transmit_fifo < 4 && transmit_state < 3)
            {
                if (transmit_state == 0)
                {
                    send_start_signal_with_address(address, RW_BIT_READ);
                }
                else if (transmit_state == 1)
                {
                    send_receive_signal_with_quantity(receive_data_quantity - 1);
                }
                else
                {
                    if (send_stop_signal_flag)
                    {
                        send_stop_signal();
                    }
                }
                transmit_state++;
                words_in_transmit_fifo++;
            }
        }
        if (receive_buffer_content_length < BUFFER_LENGTH)
        {
            uint32_t words_in_receive_fifo = get_words_in_receive_fifo();
            while (words_in_receive_fifo > 0 && receive_buffer_content_length < BUFFER_LENGTH)
            {
                receive_buffer[receive_buffer_content_length] = I2C->MRDR;
                receive_buffer_content_length++;
                words_in_receive_fifo--;
            }
            receive_buffer_global_index = 0;
        }
        uint8_t error_condition = check_transmit_receive_error(timer);
        if (error_condition != 0)
        {
            reset_receive_fifo();
            delete_time(timer);
            return error_condition;
        }
        if (transmit_state >= 3 && receive_buffer_content_length >= receive_data_quantity)
        {
            if (get_words_in_transmit_fifo() == 0 && (is_stop_detected() || !send_stop_signal_flag))
            {
                reset_receive_fifo();
                delete_time(timer);
                return 0;
            }
        }
        yield();
    }
}

uint8_t transmit_data(uint8_t address, uint8_t send_stop_signal_flag)
{
    uint8_t transmit_buffer_index = 0;
    uint8_t transmit_state = 0;
    //bus is busy so no data can be send
    if (!wait_for_bus_idle())
    {
        return 4;
    }
    uint8_t timer = generate_time();
    while (1)
    {
        if (transmit_state < 3)
        {
            uint32_t words_in_transmit_fifo = get_words_in_transmit_fifo();
            while (words_in_transmit_fifo < 4 && transmit_state < 3)
            {
                if (transmit_state == 0)
                {
                    send_start_signal_with_address(address, RW_BIT_WRITE);
                    transmit_state++;
                }
                else if (transmit_state == 1)
                {
                    if (transmit_buffer_index < transmit_buffer_content_length)
                    {
                        send_transmit_signal_with_data(transmit_buffer[transmit_buffer_index]);
                        transmit_buffer_index++;
                    }
                    else
                    {
                        transmit_state++;
                    }
                }
                else
                {
                    if (send_stop_signal_flag)
                    {
                        send_stop_signal();
                    }
                    transmit_state++;
                }
                words_in_transmit_fifo++;
            }
        }
        uint8_t error_condition = check_transmit_receive_error(timer);
        if (error_condition != 0)
        {
            delete_time(timer);
            return error_condition;
        }
        if (transmit_state >= 3)
        {
            if (get_words_in_transmit_fifo() == 0 && (is_stop_detected() || !send_stop_signal_flag))
            {
                delete_time(timer);
                return 0;
            }
        }
        yield();
    }
}

int write_data_to_transmit_buffer(uint8_t data, uint8_t new_transmission_flag)
{
    if (new_transmission_flag)
    {
        transmit_buffer_content_length = 0;
    }

    if (transmit_buffer_content_length < BUFFER_LENGTH)
    {
        transmit_buffer[transmit_buffer_content_length] = data;
        transmit_buffer_content_length++;
        return transmit_buffer_content_length;
    }
    else
    {
        return -1;
    }
}

uint8_t read_data_from_receive_buffer()
{
    if (receive_buffer_global_index < receive_buffer_content_length)
    {
        uint8_t data = receive_buffer[receive_buffer_global_index];
        receive_buffer_global_index++;
        return data;
    }
    return 0;
}

/**
 * returns the quantity of bytes in the receive buffer
 */
uint8_t get_receive_buffer_quantity(){
    return receive_buffer_content_length - receive_buffer_global_index;
}