#ifndef I2C_SENSOR_OBSERVER_INTERFACE_H
#define I2C_SENSOR_OBSERVER_INTERFACE_H

#include "../sensor_observer/sensor_observer.h"

uint8_t execute_sensor_action(uint8_t sensor_address, SENSOR_ACTION action,
                              write_received_data_function_type write_received_data_to_sensor);

#endif