#include "i2c_sensor_observer_interface.h"
#include "i2c.h"
#include "data_transfer/data_transfer.h"

void write_transmit_data(SENSOR_ACTION action)
{
    for (uint8_t i = 0; i < action.transmit_data_quantity; i++)
    {
        if (i >= 32)
        {
            break;
        }
        if(i == 0){
            write_data_to_transmit_buffer(action.transmit_data[i], NEW_TRANSMISSION);
        }
        else{
            write_data_to_transmit_buffer(action.transmit_data[i], NO_NEW_TRANSMISSION);
        }
        
    }
}

void read_received_data_from_i2c(write_received_data_function_type write_received_data_to_sensor)
{
    //read_data_from_receive_buffer();
    get_receive_buffer_quantity();
    uint8_t index = 0;
    while (get_receive_buffer_quantity() > 0)
    {
        uint8_t data = read_data_from_receive_buffer();
        (write_received_data_to_sensor)(data, index);
        index++;
    }
}

void debug(uint8_t address, SENSOR_ACTION action)
{
    i2c_string_debug_print("i2c interface debug");
    i2c_string_debug_print("address");
    i2c_uint8_debug_print(address);
    i2c_string_debug_print("action type");
    i2c_uint8_debug_print(action.action_type);
    i2c_string_debug_print("stop signal");
    i2c_uint8_debug_print(action.send_stop_signal_flag);
    if (action.action_type == ACTION_TYPE_RECEIVE || action.action_type == ACTION_TYPE_SEND_RECEIVE)
    {
        i2c_string_debug_print("receive quantity");
        i2c_uint8_debug_print(action.receive_data_quantity);
    }
    if (action.action_type == ACTION_TYPE_SEND || action.action_type == ACTION_TYPE_SEND_RECEIVE)
        i2c_string_debug_print("transmit quantity");
    i2c_uint8_debug_print(action.transmit_data_quantity);
    for (uint8_t i = 0; i < action.transmit_data_quantity; i++)
    {
        i2c_string_debug_print("transmit data");
        i2c_uint8_debug_print(action.transmit_data[i]);
    }
}

uint8_t execute_sensor_action(uint8_t sensor_address, SENSOR_ACTION action, write_received_data_function_type write_received_data_to_sensor)
{
    //debug(sensor_address, action);
    if (action.action_type == ACTION_TYPE_RECEIVE)
    {
        receive_data(sensor_address, action.receive_data_quantity, action.send_stop_signal_flag);
        read_received_data_from_i2c(write_received_data_to_sensor);
        return 0;
    }
    else if (action.action_type == ACTION_TYPE_SEND)
    {
        write_transmit_data(action);
        transmit_data(sensor_address, action.send_stop_signal_flag);
        return 0;
    }
    else if (action.action_type == ACTION_TYPE_SEND_RECEIVE)
    {
        write_transmit_data(action);
        transmit_data(sensor_address, SEND_NO_STOP_SIGNAL);
        receive_data(sensor_address, action.receive_data_quantity, action.send_stop_signal_flag);
        read_received_data_from_i2c(write_received_data_to_sensor);
        return 0;
    }
    //error case
    else
    {
        return 1;
    }
}