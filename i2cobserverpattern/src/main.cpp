#include <Arduino.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <Wire.h>
#include <stdlib.h>
#include <imxrt.h>
extern "C"
{
#include "i2c/setup/setup_util.h"
#include "i2c/data_transfer/data_transfer.h"
#include "sensor_observer/sensor_ADXL345/registers_ADXL345.h"
#include "sensor_observer/sensor_ADXL345/sensor_ADXL345.h"
#include "sensor_observer/sensor_MMA7660FC/registers_MMA7660FC.h"
#include "sensor_observer/sensor_MMA7660FC/sensor_MMA7660FC.h"
#include "i2c_subject/i2c_subject.h"
}

void determin_connected_i2c_devices_address()
{
    byte error, address = 0;
    uint8_t no_devices = 1;
    for (address = 1; address < 127; address++)
    {
        error = transmit_data(address, SEND_STOP_SIGNAL);
        if (error == 0)
        {
            Serial.print("found device with address: ");
            Serial.println(address, HEX);
            no_devices = 0;
        }
    }
    if (no_devices)
    {
        Serial.println("no devices found");
    }
}

void read_ADXL345_id()
{
    /**
     * the following does the same as the SENSOR_ACTION configuration below
     * write_data_to_transmit_buffer(ADXL345_DEVID, NEW_TRANSMISSION);
     * transmit_data(ADXL345_ADDRESS, SEND_NO_STOP_SIGNAL);
     * receive_data(ADXL345_ADDRESS, 1, SEND_STOP_SIGNAL);
     * int data = read_data_from_receive_buffer();
     **/

    SENSOR_ACTION ADXL345_action;
    ADXL345_action.action_type = ACTION_TYPE_SEND_RECEIVE;
    ADXL345_action.transmit_data_quantity = 1;
    ADXL345_action.receive_data_quantity = 1;
    ADXL345_action.send_stop_signal_flag = SEND_STOP_SIGNAL;
    ADXL345_action.transmit_data[0] = ADXL345_DEVID;
    add_sensor_ADXL345_action(ADXL345_action);
}

uint8_t setup_MMA7660FC = 1;
void read_and_write_MMA7660FC()
{
    SENSOR_ACTION MMA7660FC_action;
    if (setup_MMA7660FC)
    {
        Serial.println("activate MMA7660FC_action");
        MMA7660FC_action.action_type = ACTION_TYPE_SEND;
        MMA7660FC_action.send_stop_signal_flag = SEND_STOP_SIGNAL;
        MMA7660FC_action.transmit_data_quantity = 2;
        MMA7660FC_action.transmit_data[0] = MMA7660FC_MODE;
        MMA7660FC_action.transmit_data[1] = 0b00000001;
        add_sensor_MMA7660FC_action(MMA7660FC_action);

        setup_MMA7660FC = 0;
    }

    MMA7660FC_action.action_type = ACTION_TYPE_SEND_RECEIVE;
    MMA7660FC_action.send_stop_signal_flag = SEND_STOP_SIGNAL;
    MMA7660FC_action.receive_data_quantity = 1;
    MMA7660FC_action.transmit_data_quantity = 1;
    MMA7660FC_action.transmit_data[0] = MMA7660FC_XOUT;
    add_sensor_MMA7660FC_action(MMA7660FC_action);
}

void notify_sensor_observers()
{
    notify_sensor_observers(0);
    uint8_t ADXL345_data = get_data_ADXL345();
    Serial.print("ADXL345 ID: ");
    Serial.println(ADXL345_data, HEX);

    uint8_t MMA7660FC_data = get_data_MMA7660FC();
    Serial.print("MMA7660FC XOUT-register value ");
    Serial.println(MMA7660FC_data, BIN);
}

void setup()
{
    sei();
    init_i2c(NORMALMODE, SDA_PIN_1, SCL_PIN_1);
    add_sensor_observer(&update_sensor_ADXL345_observer);
    add_sensor_observer(&update_sensor_MMA7660FC_observer);
    Serial.begin(9600);
}

void loop()
{
    delay(3000);
    read_ADXL345_id();
    read_and_write_MMA7660FC();
    notify_sensor_observers();
}
