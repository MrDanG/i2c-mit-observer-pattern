#include "registers_ADXL345.h"
#include "sensor_ADXL345.h"
#include "../../i2c/i2c_sensor_observer_interface.h"
#include <stdlib.h>

#define BUFFER_LENGTH 32

typedef struct sensor_action_queue
{
    SENSOR_ACTION action;
    struct sensor_action_queue *next;
} SENSOR_ACTION_QUEUE;

SENSOR_ACTION_QUEUE *sensor_queue_head = NULL;

uint8_t received_data[BUFFER_LENGTH] = {0};
uint8_t received_data_quantity = 0;
uint8_t received_data_index = 0;


uint8_t enqueue_sensor_action(SENSOR_ACTION action) {
    SENSOR_ACTION_QUEUE *new_action = malloc(sizeof(SENSOR_ACTION_QUEUE));
    if(!new_action){
        return 1;
    }

    new_action->action = action;
    new_action->next = sensor_queue_head;

    sensor_queue_head = new_action;
    return 0;
}

SENSOR_ACTION dequeue_sensor_action() {
    SENSOR_ACTION_QUEUE *current_action, *previous_action = NULL;
    SENSOR_ACTION retval;

    if (sensor_queue_head == NULL){
        return retval;
    }

    current_action = sensor_queue_head;
    while (current_action->next != NULL) {
        previous_action = current_action;
        current_action = current_action->next;
    }

    retval = current_action->action;
    free(current_action);
    
    if (previous_action){
        previous_action->next = NULL;
    }
    else{
        sensor_queue_head = NULL;
    }

    return retval;
}

/**
 * DO NOT CALL THIS FUNCTION DIRECTLY
 * The function is called by the i2c mudule when data are received for this sensor
 */
uint8_t write_received_data_ADXL345(uint8_t data, uint8_t index)
{
    if (index == 0)
    {
        received_data_quantity = 0;
        received_data_index = 0;
    }
    if (index < BUFFER_LENGTH)
    {
        received_data[received_data_quantity] = data;
        received_data_quantity++;
        return 0;
    }
    return 1;
}

/**
 * DO NOT CALL THIS FUNCTION DIRECTLY
 * The function is called by the i2c_subject module when all observers are updated
 */
uint8_t update_sensor_ADXL345_observer(uint8_t arg)
{
    if(sensor_queue_head == NULL){
        return 1;
    }
    SENSOR_ACTION action = dequeue_sensor_action();
    execute_sensor_action(ADXL345_ADDRESS, action, &write_received_data_ADXL345);
    return 0;
}

/**
 * add a new sensor action to the a list of actions.
 * they can be executed by updating the (all) observers
 */
uint8_t add_sensor_ADXL345_action(SENSOR_ACTION action)
{
    return enqueue_sensor_action(action);
}

/**
 * check if data are available
 * returns the number of available data
 */
uint8_t data_available_ADXL345()
{
    return received_data_quantity - received_data_index;
}

/**
 * return the first byte of data
 */
uint8_t get_data_ADXL345()
{
    if (received_data_index < received_data_quantity && received_data_index < BUFFER_LENGTH)
    {
        uint8_t data = received_data[received_data_index];
        received_data_index++;
        return data;
    }
    return 0;
}