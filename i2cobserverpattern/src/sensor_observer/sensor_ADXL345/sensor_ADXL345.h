#ifndef ADXL345_H
#define ADXL345_H

#include "../sensor_observer.h"

uint8_t update_sensor_ADXL345_observer(uint8_t arg);
uint8_t add_sensor_ADXL345_action(SENSOR_ACTION action);
uint8_t data_available_ADXL345();
uint8_t get_data_ADXL345();

#endif