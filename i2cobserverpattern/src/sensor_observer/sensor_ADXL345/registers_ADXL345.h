#ifndef ADXL345_REGISTERS_H
#define ADXL345_REGISTERS_H

/**
 * data transfer:
 * single byte write:
 * Master->START, address + WRITE | Slave->ACK | Master->register address |
 *  Slave->ACK | Master->data | Slave->ACK | Master->STOP
 * multiple byte write:
 * Master->START, address + WRITE | Slave->ACK | Master->register address |
 *  Slave->ACK | Master->data | Slave->ACK | Master->data | Slave->ACK | Master->STOP
 * single byte read:
 * Master->START, address + WRITE | Slave->ACK | Master->register address |
 *  Slave->ACK | Master->START, address + READ | Slave->ACK | Slave->data | Master->NACK | Master->STOP
 * multiple byte read:
 * Master->START, address + WRITE | Slave->ACK | Master->register address |
 *  Slave->ACK | Master->START, address + READ | Slave->ACK | Slave->data | Master->ACK | Slave->data | Master->NACK | Master->STOP
 */

#define ADXL345_ADDRESS         0x53

#define ADXL345_DEVID           0x00
//0x01 to 0x1C
#define ADXL345_RESERVED        0x01
#define ADXL345_THRESH_TAP      0x1D
#define ADXL345_OFSX            0x1E
#define ADXL345_OFSY            0x1F
#define ADXL345_OFSZ            0x20
#define ADXL345_DUR             0x21
#define ADXL345_LATENT          0x22
#define ADXL345_WINDOW          0x23
#define ADXL345_THRESH_ACT      0x24
#define ADXL345_THRESH_INACT    0x25
#define ADXL345_TIME_INACT      0x26
#define ADXL345_ACT_INACT_CTL   0x27
#define ADXL345_THRESH_FF       0x28
#define ADXL345_TIME_FF         0x29
#define ADXL345_TAP_AXES        0x2A
#define ADXL345_ACT_TAP_STATUS  0x2B
#define ADXL345_BW_RATE         0x2C    
#define ADXL345_POWER_CTL       0x2D
#define ADXL345_INT_ENABLE      0x2E
#define ADXL345_INT_MAP         0x2F
#define ADXL345_INT_SOURCE      0x30
#define ADXL345_DATA_FORMAT     0x31
#define ADXL345_DATAX0          0x32
#define ADXL345_DATAX1          0x33
#define ADXL345_DATAY0          0x34
#define ADXL345_DATAY1          0x35
#define ADXL345_DATAZ0          0x36
#define ADXL345_DATAZ1          0x37
#define ADXL345_FIFO_CTL        0x38
#define ADXL345_FIFO_STATUS     0x39

//interrupt pins
//setting the bits in ADXL345_INT_MAP
//to 0 sends respective interrupt to INT1_PIN 
//to 1 sends respective interrupt to INT2_PIN
#define ADXL345_INT1_PIN        0x00
#define ADXL345_INT2_PIN        0x01

//interrupt settings
#define ADXL345_INT_DATA_READY  0x07
#define ADXL345_INT_SINGLE_TAP  0x06
#define ADXL345_INT_DOUBLE_TAP  0x05
#define ADXL345_INT_ACTIVITY    0x04
#define ADXL345_INT_INACTIVITY  0x03
#define ADXL345_INT_FREE_FALL   0x02
#define ADXL345_INT_WATERMARK   0x01
#define ADXL345_INT_OVERRUN     0x00

//fifo settings
#define ADXL345_FIFO_MODE_BYPASS    0x00    
#define ADXL345_FIFO_MODE_FIFO      0x01
#define ADXL345_FIFO_MODE_STREAM    0x02
#define ADXL345_FIFO_MODE_TRIGGER   0x03 

//output data rate settings
//there are more possibel settings refere to manual
//default is 0x0A
#define ADXL345_BW_3200 0x0F //1111
#define ADXL345_BW_1600 0x0E //1110
#define ADXL345_BW_800  0x0D //1101
#define ADXL345_BW_400  0x0C //1100
#define ADXL345_BW_200  0x0B //1011    
#define ADXL345_BW_100  0x0A //1010
#define ADXL345_BW_50   0x09 //1001
#define ADXL345_BW_25   0x08 //1000

#endif