#include "registers_MMA7660FC.h"
#include "sensor_MMA7660FC.h"
#include "../../i2c/i2c_sensor_observer_interface.h"
#include <stdlib.h>

#define BUFFER_LENGTH 32

typedef struct sensor_MMA7660FC_action_queue
{
    SENSOR_ACTION action;
    struct sensor_MMA7660FC_action_queue *next;
} SENSOR_MMA7660FC_ACTION_QUEUE;

SENSOR_MMA7660FC_ACTION_QUEUE *sensor_MMA7660FC_queue_head = NULL;

uint8_t received_data_MMA7660FC[BUFFER_LENGTH] = {0};
uint8_t received_data_quantity_MMA7660FC = 0;
uint8_t received_data_index_MMA7660FC = 0;


uint8_t enqueue_sensor_MMA7660FC_action(SENSOR_ACTION action) {
    SENSOR_MMA7660FC_ACTION_QUEUE *new_action = malloc(sizeof(SENSOR_MMA7660FC_ACTION_QUEUE));
    if(!new_action){
        return 1;
    }

    new_action->action = action;
    new_action->next = sensor_MMA7660FC_queue_head;

    sensor_MMA7660FC_queue_head = new_action;
    return 0;
}

SENSOR_ACTION dequeue_sensor_MMA7660FC_action() {
    SENSOR_MMA7660FC_ACTION_QUEUE *current_action, *previous_action = NULL;
    SENSOR_ACTION retval;

    if (sensor_MMA7660FC_queue_head == NULL){
        return retval;
    }

    current_action = sensor_MMA7660FC_queue_head;
    while (current_action->next != NULL) {
        previous_action = current_action;
        current_action = current_action->next;
    }

    retval = current_action->action;
    free(current_action);
    
    if (previous_action){
        previous_action->next = NULL;
    }
    else{
        sensor_MMA7660FC_queue_head = NULL;
    }

    return retval;
}

/**
 * DO NOT CALL THIS FUNCTION DIRECTLY
 * The function is called by the i2c mudule when data are received for this sensor
 */
uint8_t write_received_data_MMA7660FC(uint8_t data, uint8_t index)
{
    if (index == 0)
    {
        received_data_quantity_MMA7660FC = 0;
        received_data_index_MMA7660FC = 0;
    }
    if (index < BUFFER_LENGTH)
    {
        received_data_MMA7660FC[received_data_quantity_MMA7660FC] = data;
        received_data_quantity_MMA7660FC++;
        return 0;
    }
    return 1;
}

/**
 * DO NOT CALL THIS FUNCTION DIRECTLY
 * The function is called by the i2c_subject module when all observers are updated
 */
uint8_t update_sensor_MMA7660FC_observer(uint8_t arg)
{
    if(sensor_MMA7660FC_queue_head == NULL){
        return 1;
    }
    SENSOR_ACTION action = dequeue_sensor_MMA7660FC_action();
    execute_sensor_action(MMA7660FC_ADDRESS, action, &write_received_data_MMA7660FC);
    return 0;
}

/**
 * add a new sensor action to the a list of actions.
 * they can be executed by updating the (all) observers
 */
uint8_t add_sensor_MMA7660FC_action(SENSOR_ACTION action)
{
    return enqueue_sensor_MMA7660FC_action(action);
}

/**
 * check if data are available
 * returns the number of available data
 */
uint8_t data_available_MMA7660FC()
{
    return received_data_quantity_MMA7660FC - received_data_index_MMA7660FC;
}

/**
 * return the first byte of data
 */
uint8_t get_data_MMA7660FC()
{
    if (received_data_index_MMA7660FC < received_data_quantity_MMA7660FC && received_data_index_MMA7660FC < BUFFER_LENGTH)
    {
        uint8_t data = received_data_MMA7660FC[received_data_index_MMA7660FC];
        received_data_index_MMA7660FC++;
        return data;
    }
    return 0;
}