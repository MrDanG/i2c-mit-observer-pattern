#ifndef MMA7660FC_H
#define MMA7660FC_H

#include "../sensor_observer.h"

uint8_t update_sensor_MMA7660FC_observer(uint8_t arg);
uint8_t add_sensor_MMA7660FC_action(SENSOR_ACTION action);
uint8_t data_available_MMA7660FC();
uint8_t get_data_MMA7660FC();

#endif