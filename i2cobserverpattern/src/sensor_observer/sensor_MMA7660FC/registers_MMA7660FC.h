#ifndef MMA7660FC_REGISTERS_H
#define MMA7660FC_REGISTERS_H

#define MMA7660FC_ADDRESS   0x4C

#define MMA7660FC_XOUT      0x00
#define MMA7660FC_YOUT      0x01
#define MMA7660FC_ZOUT      0x02
#define MMA7660FC_TILT      0x03
#define MMA7660FC_SRST      0x04
#define MMA7660FC_SPCNT     0x05
#define MMA7660FC_INTSU     0x06
#define MMA7660FC_MODE      0x07
#define MMA7660FC_SR        0x08
#define MMA7660FC_PDET      0x09
#define MMA7660FC_PD        0x0A

#endif