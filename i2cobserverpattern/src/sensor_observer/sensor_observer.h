#ifndef SENSOR_OBSERVER_H
#define SENSOR_OBSERVER_H

#include <stdint.h>

/**
 * action_type -- determines how the action is handled
 *  possible values are ACTION_TYPE_SEND, ACTION_TYPE_RECEIVE, ACTION_TYPE_SEND_RECEIVE
 * receive_data_quantity -- number of bytes to receive 
 *  only with ACTION_TYPE_RECEIVE, ACTION_TYPE_SEND_RECEIVE
 * send_stop_signal_flag -- determines whether or not a stop signal is send
 * transmit_data_quantity -- the number of bytes in transmit_data
 *  should only be used in combination with transmit_data
 * transmit_data -- holds the data to transmit, max 32 byte
 *  only with ACTION_TYPE_SEND, ACTION_TYPE_SEND_RECEIVE and in combination with transmit_data_quantity
 */
typedef struct sensor_action
{
    uint8_t action_type;
    uint8_t receive_data_quantity;
    uint8_t send_stop_signal_flag;
    uint8_t transmit_data_quantity;
    uint8_t transmit_data[32];
} SENSOR_ACTION;

#define ACTION_TYPE_SEND            1
#define ACTION_TYPE_RECEIVE         2
#define ACTION_TYPE_SEND_RECEIVE    3

typedef uint8_t (*write_received_data_function_type)(uint8_t data, uint8_t index);

uint8_t update_sensor_observer(uint8_t arg);
uint8_t write_received_data(uint8_t data, uint8_t index);

#endif