#include<stdlib.h>
#include "subject.h"

//dynamic array of subscribed observers
update_function_type *observer_array;
//number of subscribed observers
int subscribed_observers = 0;

/**
 * This function adds an observer to the array of observers
 */
int add_observer(update_function_type observer_to_add){
    //if there is no subscribed observer yet allocate memory for observer array
    if(subscribed_observers == 0){
        observer_array = malloc(sizeof(update_function_type));
    }
    //if there are subscribed observers reallocate memory for observer array
    else{
        observer_array = realloc(observer_array, ((subscribed_observers + 1) * sizeof(update_function_type)));
    }

    //add new observer to array and increase number of subscribed observers
    observer_array[subscribed_observers] = observer_to_add;
    subscribed_observers++;

    return 0;
}

/**
 * This function removes an observer from the array of observers
 */
int remove_observer(update_function_type observer_to_remove){
    //determine index of observer that should be removed
    int index_of_observer_to_remove = -1;
    for(int i = 0; i < subscribed_observers; i++){
        if(observer_array[i] == observer_to_remove){
            index_of_observer_to_remove = i;
            break;
        }
    }

    //new observer array
    update_function_type *new_observer_array;
    new_observer_array = malloc(((subscribed_observers - 1) * sizeof(update_function_type)));

    //copy old observer array except the one that should be removed
    int new_observer_array_index = 0;
    for(int i = 0; i < subscribed_observers; i++){
        //skip the one that should be removed
        if(i == index_of_observer_to_remove){
            continue;
        }
        new_observer_array[new_observer_array_index] = observer_array[i];
        new_observer_array_index++;
    }

    //free memory of old observer array
    free(observer_array);
    //set observer array to new observer array
    observer_array = new_observer_array;
    
    subscribed_observers--;

    return 0;
}

/**
 * This function notifies all subscribed observers by calling their notify-function
 */
int notify_observers(const char *args[]){
    for(int i = 0; i < subscribed_observers; i++){
        (*observer_array[i])(args);
    }

    return 0;
}

/**
 * This function frees the memory allocated by the observer array
 */
int cleanup_subject(){
    free(observer_array);

    return 0;
}