#include <stdio.h>
#include <stdlib.h>
#include "subject.h"
#include "myHeader.h"

int main(int argc, char const *argv[]){
    notify_observers(NULL);
    add_observer(&update_observer_a);
    notify_observers(NULL);
    add_observer(&update_observer_b);
    notify_observers(NULL);
    add_observer(&update_observer_c);
    notify_observers(NULL);

    remove_observer(&update_observer_b);
    notify_observers(NULL);
    add_observer(&update_observer_b);
    notify_observers(NULL);

    remove_observer(&update_observer_a);
    notify_observers(NULL);
    add_observer(&update_observer_a);
    notify_observers(NULL);

    remove_observer(&update_observer_a);
    notify_observers(NULL);

    cleanup_subject();

    return 0;
}

