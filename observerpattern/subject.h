#ifndef SUBJECT_H
#define SUBJECT_H

typedef int (*update_function_type)(const char *args[]);

int add_observer(update_function_type observer_to_add);
int remove_observer(update_function_type observer_to_remove);
int notify_observers(const char *args[]);
int cleanup_subject();

#endif