#ifndef OBSERVER_H
#define OBSERVER_H

int update_observer_a(const char *args[]);
int update_observer_b(const char *args[]);
int update_observer_c(const char *args[]);

#endif